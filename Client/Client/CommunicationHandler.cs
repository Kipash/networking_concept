﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Client
{
    public delegate void OnEvent(byte id, int token, byte[] payload); //Change to delegate
    public class CommunicationHandler
    {
        int protocolLevel; //Defines what the read data represent (i.e. am i expecting OpenFlag or ID)
        byte[] smallBuffer = new byte[4];
        byte[] readBuffer = new byte[256 * 256]; //68 KB


        public Action<int> OnActivity;
        public OnEvent OnEvent;
        public Queue<Event> CommandQueue = new Queue<Event>();

        byte currentID = 0;
        int currentLength = -1;


        int currentToken = -1;
        int bytesRead = 0;
        byte[] currentPayload;

        bool handleUser_activeLastCall;
        public void HandleServer(TcpClient tcpClient, ref DateTime lastBeat)
        {
            var stream = tcpClient.GetStream();

            if (stream.DataAvailable)
            {
                handleUser_activeLastCall = true;
                lastBeat = DateTime.Now;

                Debug.Write("\nReading handle cycle: ");
                if (protocolLevel == 0) //255 - Open flag
                {
                    var readBytes = stream.Read(smallBuffer, 0, 1);
                    if (readBytes != 1 && smallBuffer[0] != 255)
                        Debug.ErrorLine($"Reading error: not ID {readBytes:N}");
                    else
                    {
                        Debug.Write(" O ");
                        protocolLevel++;
                    }
                }
                else if (protocolLevel == 1) //ID
                {
                    var readBytes = stream.Read(smallBuffer, 0, 1);
                    if (readBytes != 1)
                        Debug.ErrorLine($"Reading error: not ID {readBytes:N}");
                    else
                    {
                        currentID = smallBuffer[0];
                        protocolLevel++;
                        Debug.Write($" ID {(EventType)currentID} ");
                    }
                }
                else if (protocolLevel == 2) //Token
                {
                    var readBytes = stream.Read(smallBuffer, 0, 4);
                    if (readBytes != 4)
                        Debug.ErrorLine($"Reading error: not token {readBytes:N}");
                    else
                    {
                        currentToken = BitConverter.ToInt32(smallBuffer);
                        Debug.Write($" T {currentToken} \n");
                        // Check if we expect such Token + if event of such ID needs to be requested with a token
                        protocolLevel++;
                    }
                }
                else if (protocolLevel == 3) //Length
                {
                    var readBytes = stream.Read(smallBuffer, 0, 4);
                    if (readBytes != 4)
                        Debug.ErrorLine($"Reading error: not length {readBytes:N}");
                    else
                    {
                        currentLength = BitConverter.ToInt32(smallBuffer);
                        Debug.Write($" L {currentLength} \n");
                        currentPayload = new byte[currentLength]; //TODO: inefficient
                        protocolLevel++;
                    }
                }
                else if (protocolLevel == 4) //Payload
                {
                    OnActivity?.Invoke(currentToken);

                    var remaining = currentLength - bytesRead;
                    var bufferSize = remaining < readBuffer.Length ? remaining : readBuffer.Length;

                    var bytes = stream.Read(readBuffer, 0, bufferSize);
                    for (int i = 0; i < bytes; i++)
                    {
                        currentPayload[bytesRead + i] = readBuffer[i];
                    }
                    bytesRead += bytes;
                    Debug.WriteLine($" => Reading {bytes:N} / {bytesRead:N} / {currentLength:N}");


                    if (currentLength == bytesRead)
                    {
                        //currentPayload = buffer;
                        protocolLevel++;
                    }
                    else if (currentLength < bytesRead)
                    {
                        Debug.ErrorLine($"Reading error, payload reading exceeded... excpected bytes {currentLength:N}, recieved bytes {bytesRead:N}");
                    }
                }
                else if (protocolLevel == 5) //254 - Close flag
                {
                    Debug.Write(" E \n");
                    var readBytes = stream.Read(smallBuffer, 0, 1);
                    if (readBytes != 1 && smallBuffer[0] != 254)
                        Debug.ErrorLine($"Reading error: not ID {readBytes}");
                    else
                    {
                        OnEvent(currentID, currentToken, currentPayload);
                        ResetProtocol();
                    }
                }
            }
            else if (CommandQueue.Count > 0) //Otherwise write
            {
                //TODO: remove

                var eventData = CommandQueue.Peek();
                byte[] sendBuffer = new byte[eventData.Size];

                byte[] bytes = eventData.Payload;

                var token = BitConverter.GetBytes(eventData.Token);

                var size = DataUtility.AddAt(sendBuffer, 0, eventData.ID, token, bytes);

                stream.Write(sendBuffer, 0, size);
                Debug.WriteLine($"Sending ... {eventData.PayloadLength} ({size})");
                CommandQueue.Dequeue(); //If the Write throws exception, data would be lost
            }
            else
            {
                handleUser_activeLastCall = false;
            }

            if (!handleUser_activeLastCall)
                Thread.Sleep(100);
        }

        public void ResetProtocol()
        {
            currentID = 0;
            currentToken = -1;
            bytesRead = 0;
            currentPayload = null;
            currentLength = -1;
            bytesRead = 0;
            protocolLevel = 0;
        }

        //static bool communicationDebug = true;
        //void WriteLine(string s) 
        //{ 
        //    if(communicationDebug)
        //        Console.WriteLine(s); 
        //}
        //void Write(string s) 
        //{ 
        //    if(communicationDebug)
        //        Console.Write(s); 
        //}
    }

    public class Event
    {
        public bool IsRequest; //true - request, false - response | Decides if token is generated or provided 
        public bool ExpectResponse;
        public bool Unique; //true - no more then one request of same ID can be sent and be pending | false - all events of the same ID are sent

        public EventType EventType { set { ID = (byte)value; } } // Helper for not dealing with numbers
        public byte ID;
        public int Token;
        public int PayloadLength => Payload.Length;
        public byte[] Payload;

        public int Size => 1 + 4 + 4 + PayloadLength + 2; // Size of the actual transmitted data, ID, Token, Length, Payload, Open/Close flag

        public double TimeoutCoefficient = 1; // Higher event's life time

        public DateTime LastActivity; // When reading this is updated, used for resending events if they are never confirmed

        public DateTime CreationTime; // When the packet was created

        public Action OnConfirmed; // Fires when confirmation event with the same token arrives

        public Event()
        {
            LastActivity = CreationTime = DateTime.Now;
        }
    }
}
