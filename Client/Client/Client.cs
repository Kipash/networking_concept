﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml.Schema;

namespace Client
{
    public enum EventType : byte  // 1-255!  => 0 is error state
    {
        //Essential events
        Authentication = 1,
        HeartBeat,
        Confirmation,

        //Exact events
        //RequestContent = 50,
        //AcceptContet,

        RequestContentManifest = 80,
        RequestTranslationManifest,
        RequestFace,
        RequestImage,
        RequestContent,


        ResponseContentManifest = 150,
        ResponseTranslationManifest,
        ResponseFace,
        ResponseImage,
        ResponseContent,

        //For testing purposes
        Message = 250,
    }

    public class Client
    {
        TcpClient tcpClient;
        NetworkStream stream;

        CommunicationHandler Handler;

        public bool Connected;
        bool hadTimeout;

        public DateTime LastBeat = DateTime.Now;

        public Client()
        {
            RegisterHanlders();
        }

        //public void RequestContentManifest()
        //{
        //
        //}

    

        public void SendHeartBeat()
        {
            AddEvent(new Event()
            {
                EventType = EventType.HeartBeat,
                IsRequest = true,
                ExpectResponse = false,
                Payload = new byte[] { 0 },
                Unique = true,
                OnConfirmed = () => LastBeat = DateTime.Now,
            });
        }

        public void Timeout()
        {
            hadTimeout = true;
            Connected = false;

            //On timeout
        }

        public void ServerConnection(string rawIP)
        {
            while (true)
            {
                Debug.InfoLine("reconnecting ...");

                if (ConnectToServer(rawIP))
                {
                    Debug.WriteLine("Handle server loop starting...");

                    Handler = new CommunicationHandler();
                    Handler.OnEvent = OnEvent;
                    Handler.OnActivity = RegisterTokenActivity;
                    Connected = true;

                    Authenticate();
                    ConnectionEstabilished();
                    HandleServerLoop();

                    Debug.WriteLine("Handle server loop stopped...");
                }
                else
                { Debug.ErrorLine("Can't connect"); }
                Connected = false;
                Thread.Sleep(2000);

                Debug.InfoLine("restarting");
            }
        }
        void Authenticate()
        {
            Console.WriteLine(Handler.CommandQueue.Count);
            AddEvent(new Event()
            {
                EventType = EventType.Authentication,
                IsRequest = true,
                ExpectResponse = false,
                Unique = true,
                Payload = Program.ID.ToByteArray(),
            }, true);
            Console.WriteLine(Handler.CommandQueue.Count);

        }

        void TryToExecute(Action a)
        {
            try { a(); }
            catch(Exception e) { Debug.ErrorLine($"TryToExeute error: {e.GetType().Name}\n{e.Message}\n{e.StackTrace}"); }
        }

        /// <summary>
        /// Establishes a connection to the server
        /// </summary>
        /// <returns>true if connection was successful</returns>
        bool ConnectToServer(string rawIP)
        {
            try
            {
                Debug.WriteLine("Preparing to connect...");
                if (tcpClient != null && tcpClient.Connected) //In case of dropped connection
                {
                    Debug.WriteLine("Killing old connection...");

                    var oldSocket = tcpClient.Client;
                    if (oldSocket.Connected && oldSocket.Connected && oldSocket.Connected && oldSocket.Connected && oldSocket.Connected && oldSocket.Connected)
                    {
                        Console.WriteLine("1");
                        TryToExecute(() => oldSocket.Shutdown(SocketShutdown.Both));
                        Console.WriteLine("2");
                        TryToExecute(() => { if (oldSocket.Connected) { oldSocket.Disconnect(true); } });
                        Console.WriteLine("3");
                        TryToExecute(() => oldSocket.Close());
                        Console.WriteLine("4");
                        TryToExecute(() => oldSocket.Dispose());
                        Console.WriteLine("5");
                        TryToExecute(() => oldSocket.Dispose());

                        Console.WriteLine("6");
                        TryToExecute(() => tcpClient.GetStream().Close());
                    }
                    Console.WriteLine("7");
                    TryToExecute(() => tcpClient.Close());
                    Console.WriteLine("8");
                    TryToExecute(() => tcpClient.Dispose());

                    Debug.WriteLine("Old connection killed...");
                }

                Debug.WriteLine("Setting up end point...");
                var ip = IPAddress.Parse(rawIP);
                var port = 5678;
                IPEndPoint ipEndPoint = new IPEndPoint(ip, port);

                const int timeout = 1000; //not working at all >:(

                tcpClient = new TcpClient();
                tcpClient.SendTimeout = timeout;
                //Console.WriteLine(tcpClient.SendTimeout);
                tcpClient.ReceiveTimeout = timeout;
                //Console.WriteLine(tcpClient.ReceiveTimeout);

                Debug.WriteLine("Connecting...");
                tcpClient.Connect(ipEndPoint);
                Debug.WriteLine("Connected...");

                var socket = tcpClient.Client;
                socket.ReceiveTimeout = timeout;
                //Console.WriteLine(socket.ReceiveTimeout);
                socket.SendTimeout = timeout;
                //Console.WriteLine(socket.SendTimeout);

                stream = tcpClient.GetStream();
                stream.ReadTimeout = timeout;
                //Console.WriteLine(stream.ReadTimeout);
                stream.WriteTimeout = timeout;
                //Console.WriteLine(stream.WriteTimeout);

                Console.Title = $"CLIENT ({tcpClient.Client.RemoteEndPoint})";
                Debug.WriteLine("Connected");
                return true;
            }
            catch (Exception e)
            {
                Debug.ErrorLine($"Connection error: {e.GetType().Name} - {e.Message}\n\n{e.StackTrace}");
            }

            return false;
        }
        void HandleServerLoop()
        {
            while (true)
            {
                try
                {
                    if (!Connected)
                        throw new Exception("Connection to the Server was interrupted.");

                    Handler.HandleServer(tcpClient, ref LastBeat);
                }
                catch (Exception e)
                {
                    Handler.ResetProtocol();
                    Debug.ErrorLine($"Handle server loop error: {e.GetType().Name}\n{e.Message}\n{e.StackTrace}");
                    break;
                }
            }
        }

        void ConnectionEstabilished()
        {
            LastBeat = DateTime.Now;

            //Handle timeout related errors => resend old requests
            if(hadTimeout)
            {

            }
            else
            {
                //Brand new connection
            }


            hadTimeout = false;
        }

       

        #region handlers

        Dictionary<EventType, Action<int, byte[]>> handlers = new Dictionary<EventType, Action<int, byte[]>>();

        //Dictionary<int, Event> eventHistory = new Dictionary<int, Event>();
        Dictionary<int, Event> requests = new Dictionary<int, Event>();
        Dictionary<int, Event> responses = new Dictionary<int, Event>();

        Random random = new Random();

        void RegisterHanlders()
        {
            handlers.Add(EventType.Confirmation, HandleConfirmation);

            //handlers.Add(EventType.AcceptContet, HandleAceptContent);
            //handlers.Add(EventType.Message, HandleMessage);
        }

        const double EventTimeout = 15;


        //Event timeout
        public void CheckEventHistory()
        {
            List<Event> toReschedule = new List<Event>(); // :(
            List<Event> toRemove = new List<Event>(); // :(
            foreach(var x in requests.Values)
            {
                if (DateTime.Now.Subtract(x.LastActivity).TotalSeconds > EventTimeout * x.TimeoutCoefficient)
                {
                    if (IsEvent(x.ID, EventType.HeartBeat, EventType.Authentication)) //Events which shouldn't get rescheduled
                        toRemove.Add(x);
                    else
                        toReschedule.Add(x);
                }
            }

            foreach(var x in toReschedule)
            {
                Debug.WarningLine($"Event timeout - rescheduling - ID: {(EventType)x.ID}, T: {x.Token}, L: {x.PayloadLength}");
                requests.Remove(x.Token);
                x.Token = 0;
                AddEvent(x);
            }

            foreach(var x in toRemove)
            {
                Debug.WarningLine($"Removing - - ID: {(EventType)x.ID}, T: {x.Token}, L: {x.PayloadLength}");
                requests.Remove(x.Token);
            }


            Console.Title = $"CLIENT Q: {Handler.CommandQueue.Count}, Req: {requests.Count}, Res: {responses.Count}";
        }

        public void AddEvent(Event e, bool force = false)
        {
            var q = Handler.CommandQueue;

            if (IsEvent(e.ID, EventType.Confirmation)) //Create a flag for non confirmable events
            {
                q.Enqueue(e);
                return;
            }

            if(e.Unique)
            {
                if (requests.Values.Any(x => x.ID == e.ID) || responses.Values.Any(x => x.ID == e.ID)) //Performance?   
                {
                    Debug.ErrorLine("Anti-spam prevention");
                    return;
                }
            }

            if (e.IsRequest)
            {
                if (requests.ContainsValue(e))
                    requests.Remove(e.Token);

                if (e.Token == 0)
                    e.Token = random.Next(0, int.MaxValue);

                requests.Add(e.Token, e);
            }
            else
            {
                if (responses.ContainsValue(e))
                    responses.Remove(e.Token);

                responses.Add(e.Token, e);
            }

            if(force) //Forcing some evernts in front
            {
                var oldQ = q.ToArray();
                Console.WriteLine($"old: {oldQ.Length}");
                q = new Queue<Event>();
                q.Enqueue(e);
                foreach(var x in oldQ)
                {
                    if (IsEvent(x.ID, EventType.Confirmation, EventType.HeartBeat))
                        continue;

                    q.Enqueue(x);
                }
                Console.WriteLine($"new: {q.Count}");

                Handler.CommandQueue = q;
            }
            else
                q.Enqueue(e);

        }

        public void RegisterHandler(EventType type, Action<int, byte[]> handler)
        {
            handlers.Add(type, handler);
        }

        void RegisterTokenActivity(int Token)
        {
            if(requests.TryGetValue(Token, out Event e))
            {
                e.LastActivity = DateTime.Now;
            }
        }

        void OnEvent(byte ID, int token, byte[] payload)
        {
            var type = Enum.ToObject(typeof(EventType), ID);
            if (type != null) 
            {
                handlers[(EventType)type](token, payload);
            }
            else
            {
                Debug.WarningLine($"Unrecognized event ID: {ID}");
            }
        }

        /// <summary>
        /// Sends a confirmation event
        /// </summary>
        /// <param name="token"></param>
        /// <param name="sucess"></param>
        /// <param name="role">false - response, true - request | POV of the sender</param>
        public void SendConfirm(int token, bool sucess, bool role)
        {
            AddEvent(new Event()
            {
                EventType = EventType.Confirmation,
                IsRequest = role,
                ExpectResponse = false,
                Token = token,
                Payload = new byte[] { (byte)(sucess ? 1 : 0), (byte)(role ? 1 : 2) },
            });
        }
        public void Confirm(int token)
        {
            if (!requests.Remove(token))
                Debug.WarningLine($"Token {token} can't be confirmed and request can't be removed.");
        }

        //void HandleMessage(int token, byte[] payload)
        //{
        //    //Temp
        //    Debug.InfoLine($"Server> Message: {Encoding.ASCII.GetString(payload)}");
        //    Confirm(token, true, true);
        //}
        //void HandleAceptContent(int token, byte[] payload)
        //{
        //    //Temp
        //    Debug.InfoLine($"Payload arrived: {payload.TakeWhile(x => x == 0).Count()} | {string.Join(',', payload.Take(100))} ...\n {string.Join(',', payload.TakeLast(100))}");
        //    Confirm(token, true, false);
        //
        //    requests.Remove(token);
        //}

        void HandleConfirmation(int token, byte[] payload)
        {
            Debug.InfoLine("Conf");
            if(payload.Length == 2)
            {
                var ok = payload[0];
                if(!(ok == 0 || ok == 1))
                {
                    Debug.ErrorLine($"Response state is invalid {ok}");
                    return;
                }

                var role = payload[1];

                Dictionary<int, Event> events = null;
                if (role == 1)
                    events = requests;
                else if (role == 2)
                    events = responses;

                if (events != null)
                {
                    if (events.TryGetValue(token, out Event eventData))
                    {
                        if (ok == 1)
                        {
                            if(IsEvent(eventData.ID, EventType.RequestContent, EventType.Message))
                            {
                                Debug.InfoLine($"Relevant event confirmed {(EventType)eventData.ID}, {role}, Expect Response: {eventData.ExpectResponse}");
                            }

                            eventData.OnConfirmed?.Invoke();
                            if (role == 2)
                            {
                                requests.Remove(token);
                                responses.Remove(token);
                                
                                Debug.InfoLine("Removing from all buffers");
                            }
                            else
                            {
                                if (!eventData.ExpectResponse)
                                    events.Remove(token);
                            }
                        }
                        else
                        {
                            eventData.Token = 0; //will be regenerated
                            AddEvent(eventData);
                            Debug.WarningLine("Event was unsuccessful ... resending!");
                        }
                    }
                    else
                        Debug.ErrorLine($"Unknown event has {(ok == 0 ? "unsuccessful" : "successful")} confirmation"); //?
                }
                else
                    Debug.ErrorLine($"Unknown role {role}");
            }
            else
                Debug.ErrorLine($"Confirmation has invalid payload of length {payload.Length}");
        }

        #endregion
        
        
        bool IsEvent(byte ID, params EventType[] types)
        {
            foreach(var x in types)
            {
                if (ID == (byte)x)
                    return true;
            }
            return false;
        }
    }
}
