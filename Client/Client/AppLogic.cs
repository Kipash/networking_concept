﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Client
{
    public class AppLogic
    {
        Dictionary<string, ContentData> loadedContent = new Dictionary<string, ContentData>();

        Client client;
        public AppLogic(Client c)
        {
            client = c;

            c.RegisterHandler(EventType.ResponseContentManifest, Handle_Response_ContentManifest);
            c.RegisterHandler(EventType.ResponseContent, Handle_Response_Content);

            c.RegisterHandler(EventType.ResponseFace, Handle_Response_Face);
        }

        void Handle_Response_Face(int token, byte[] payload)
        {
            if(payload.Length < 2)
            {
                Debug.ErrorLine("No head received!");
                return;
            }

            client.Confirm(token);
            client.SendConfirm(token, true, false);

            var head = DataUtility.ReadList(payload, 0);
            if (head.Length == 2)
            {
                var texture = head[0];
                Console.WriteLine(texture.Length);
                var mesh = head[1];
                Console.WriteLine(mesh.Length);
            }
            else
                Debug.ErrorLine($"Response face has invalid payload, expecting 2 parts, {head.Length} read");
        }

        void Handle_Response_Content(int token, byte[] payload)
        {
            //Consider automating it => success based on bool return value etc.
            client.Confirm(token);
            client.SendConfirm(token, true, false);

            Debug.WarningLine($"Content recieved: {payload.Length}");
        }

        void Handle_Response_ContentManifest(int token, byte[] payload)
        {
            //Consider automating it => success based on bool return value etc.
            client.Confirm(token);
            client.SendConfirm(token, true, false);


            Console.WriteLine(string.Join(',', payload.Take(100)));

            var objects = DataUtility.ReadList(payload, 0);
            foreach(var contentInfo in objects)
            {
                var list = DataUtility.ReadList(contentInfo, 0);
                if (list.Length == 4)
                {
                    var _ID = list[0];
                    var _Target = list[1];
                    var _Type = list[2];
                    var _AccessLevel = list[3];

                    string ID = Encoding.ASCII.GetString(_ID);
                    //Target ... 
                    var Type = BitConverter.ToInt32(_Type);
                    var AccessLevel = BitConverter.ToInt32(_AccessLevel);

                    loadedContent.Add(ID, new ContentData()
                    {
                        ID = ID,
                        Target = _Target,
                        AccessLevel = AccessLevel,
                        Type = Type
                    });
                }
                else
                    Debug.ErrorLine($"Content manifest has incorrect size of {objects.Length}");
            }
        }

        public void OnKey(ConsoleKeyInfo key)
        {
            if (key.Key == ConsoleKey.Q)
            {
                Debug.InfoLine($"Content manifest({loadedContent.Count}): ");
                foreach(var x in loadedContent.Values)
                {
                    Debug.WriteLine($"{x.ID} - {x.Type}");
                }
            }
            if (key.Key == ConsoleKey.W)
            {
                client.AddEvent(new Event()
                {
                    EventType = EventType.RequestContentManifest,
                    IsRequest = true,
                    ExpectResponse = true,
                    Unique = true,
                    Payload = new byte[] { 0 }
                });
            }
            if (key.Key == ConsoleKey.E)
            {
                client.AddEvent(new Event()
                {
                    EventType = EventType.RequestContent,
                    IsRequest = true,
                    ExpectResponse = true,
                    Unique = true,
                    Payload = Encoding.ASCII.GetBytes("Example_TEST_1"),
                });
            }
            if (key.Key == ConsoleKey.R)
            {
                client.AddEvent(new Event()
                {
                    EventType = EventType.RequestContent,
                    IsRequest = true,
                    ExpectResponse = true,
                    Unique = true,
                    Payload = Encoding.ASCII.GetBytes("Example_TEST_2"),
                });
            }
            if (key.Key == ConsoleKey.T)
            {
                client.AddEvent(new Event()
                {
                    EventType = EventType.RequestContent,
                    IsRequest = true,
                    ExpectResponse = true,
                    Unique = true,
                    Payload = Encoding.ASCII.GetBytes("Example_TEST_3"),
                });
            }
            if (key.Key == ConsoleKey.Y)
            {
                var b = File.ReadAllBytes(@"C:\Users\Krystof\Downloads\20200704_174914.jpg");
                Console.WriteLine();
                Console.WriteLine(b.Length);
                Console.WriteLine();
                client.AddEvent(new Event()
                {
                    EventType = EventType.RequestFace,
                    IsRequest = true,
                    ExpectResponse = true,
                    TimeoutCoefficient = 10, 
                    Unique = true,
                    Payload = b
                });
            }
        }
    }

    public class ContentData
    {
        public string ID;
        public object Target; 
        public int Type;
        public int AccessLevel;

        public bool HasContentDownloaded;
        public object Content; 
    }
}
