﻿using System;
using System.Threading;
using System.Text;

namespace Client
{
    class Program
    {
        //MOCK GUID
        static Guid[] guids = new Guid[] { Guid.Parse("92fdd43f-d1b4-4556-af60-811bd5e87def"), Guid.Parse("f64bebcf-8a97-41b5-82c5-1bda7416e3fb"), Guid.Parse("f827e20f-f447-4927-8dea-fb71a9b5ae86"), Guid.Parse("b2d60144-a368-4e3c-8181-c4a5bfc9405f"), Guid.Parse("fddbfe9e-5c54-43a1-b504-d4350fbaf466") };
        public static Guid ID = Guid.Empty;

        const double Timeout = 7000;

        static void Main(string[] args)
        {
            var c = new Client();
            var logic = new AppLogic(c);

            //Console.WriteLine("IPv4: ");
            var ipText = "192.168.0.100";//Console.ReadLine();
            Console.WriteLine("ID (1-5): ");
            var idIndex = Console.ReadLine();

            ID = guids[4]; //defualt
            if (idIndex == "1")
                ID = guids[0];
            if (idIndex == "2")
                ID = guids[1];
            if (idIndex == "3")
                ID = guids[2];
            if (idIndex == "4")
                ID = guids[3];
            if (idIndex == "5")
                ID = guids[4];
            

            Console.Title = "CLIENT";
            Thread.CurrentThread.Name = "CLIENT_MainLoop";

            var serverComThread = new Thread(() => c.ServerConnection(ipText));
            serverComThread.Name = "CLIENT_Server communication";
            serverComThread.Start();

            var heartBeat = new Timer(new TimerCallback((o) => {
                if (c.Connected)
                {
                    c.SendHeartBeat(); //C -> S

                    if(DateTime.Now.Subtract(c.LastBeat).TotalMilliseconds >= Timeout) //C <- S
                    {
                        c.Timeout();
                    }

                    c.CheckEventHistory();
                }
            }), null, 1000, 2000);

            while (true) 
            {
                try
                {
                    var key = Console.ReadKey(true);
                    logic.OnKey(key);
                }
                catch(Exception e)
                {
                    Debug.ErrorLine($"{e.GetType().Name}\n{e.Message}\n{e.StackTrace}");
                }
            }
        }

    }

    public class Debug
    {
        static ConsoleColor infoCol = ConsoleColor.White;
        static ConsoleColor infoColReduced = ConsoleColor.DarkGray;
        static ConsoleColor errorCol = ConsoleColor.DarkRed;
        static ConsoleColor warningCol = ConsoleColor.DarkYellow;

        public static void WarningLine(string s) { SetCol(warningCol); Console.WriteLine(s); }
        public static void Warning(string s) { SetCol(warningCol); Console.Write(s); }

        public static void ErrorLine(string s) { SetCol(errorCol); Console.WriteLine($"{Thread.CurrentThread.Name}\n\n{s}"); }
        public static void Error(string s) { SetCol(errorCol); Console.Write(s); }

        public static void InfoLine(string s) { SetCol(infoCol); Console.WriteLine(s); }
        public static void Info(string s) { SetCol(infoCol); Console.Write(s); }

        public static void WriteLine(string s) { SetCol(infoColReduced);  Console.WriteLine(s); }
        public static void Write(string s) { SetCol(infoColReduced);  Console.Write(s); }


        static void SetCol(ConsoleColor col)
        {
            Console.ForegroundColor = col;
        }
    }

    //Temp helper to convert text to bytes / bytes to text 
    public class Conv
    {
        public static byte[] Text(string s) => Encoding.ASCII.GetBytes(s);
        public static string Text(byte[] s) => Encoding.ASCII.GetString(s);
    }
}
