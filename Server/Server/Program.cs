﻿using System;
using System.Threading;
using System.Text;
using Server.Manifest;
using Server.Content;
using Server.AvatarSDK;
using Server.Networking;

namespace Server
{
    class Program
    {
        const double TimeOut = 10f;
        static DateTime emptyTime = new DateTime(); //Used for timing out events

        static void Main(string[] args)
        {
            Debug.InfoLine("Loading & creating data ..." );

            var us = new UserServer();
            var gs = new GreenScreenServer();
            var mm = new ManifestManager();
            var cm = new ContentManager(us);
            var am = new AvatarManager(mm.AppManifest, us);

            cm.LoadSamples(mm.SampleInfos);
            
            

            Console.Title = $"SERVER";
            Debug.InfoLine("Starting the server...");

            us.StartServer(mm.AppManifest.UserPort);
            var usT = new Thread(us.Listen);
            usT.Name = "SERVER_USER_Incoming connections";
            usT.Start();

            gs.StartServer(mm.AppManifest.GreenScreenPort);
            var gsT = new Thread(us.Listen);
            gsT.Name = "SERVER_GREENSCREEN_Incoming connections";
            gsT.Start();

            var heartBeat = new Timer(new TimerCallback((o) => {
                
                //Timeout inactive users
                foreach (var user in us.Users.Values)
                {
                    var t = DateTime.Now.Subtract(user.LastBeat);
                    if(user.IsConnected)
                    {
                        if(t.TotalSeconds >= TimeOut) // C -> S
                            us.HandleConnectionClosed(user);
                    }

                    Console.Title = $"SERVER Q: {user.EventQueue.Count}, Req: {user.Requests.Count}, Res: {user.Responses.Count}";
                }
            }), null, 1000, 2000);
                
            while (true)
            {
                Console.ReadKey(true);

                foreach (var x in us.Users.Values)
                {
                    if (x.IsConnected)
                    { 
                        //x.RegisterEvent(new Event()
                        //{
                        //    EventType = UserEventType.Message,
                        //    IsRequest = true,
                        //    ExpectResponse = false,
                        //    Payload = Conv.Text($"{x.ID}: {x.LastBeat.ToLongTimeString()}"),
                        //});
                    }
                }
            }
        }
    }

    public class Debug
    {
        static ConsoleColor infoCol = ConsoleColor.White;
        static ConsoleColor infoColReduced = ConsoleColor.DarkGray;
        static ConsoleColor errorCol = ConsoleColor.DarkRed;
        static ConsoleColor warningCol = ConsoleColor.DarkYellow;

        static public void WarningLine(string s) { SetCol(warningCol); Console.WriteLine(s); }
        static public void Warning(string s) { SetCol(warningCol); Console.Write(s); }

        static public void ErrorLine(string s) { SetCol(errorCol); Console.WriteLine(s); }
        static public void Error(string s) { SetCol(errorCol); Console.Write(s); }

        static public void InfoLine(string s) { SetCol(infoCol); Console.WriteLine(s); }
        static public void Info(string s) { SetCol(infoCol); Console.Write(s); }

        static public void WriteLine(string s) { SetCol(infoColReduced); Console.WriteLine(s); }
        static public void Write(string s) { SetCol(infoColReduced); Console.Write(s); }


        static void SetCol(ConsoleColor col)
        {
            Console.ForegroundColor = col;
        }
    }

    //Temp helper to convert text to bytes / bytes to text 
    public class Conv
    {
        public static byte[] Text(string s) => Encoding.ASCII.GetBytes(s);
        public static string Text(byte[] s) => Encoding.ASCII.GetString(s);
    }
}
