﻿using System;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
    public class CommunicationHandler
    {
        public User User;
        public TcpClient Client;
        public CommunicationHandler(TcpClient client)
        {
            Client = client;
        }

        int protocolLevel; //Defines what the read data represent (i.e. am i expecting OpenFlag or ID)
        byte[] smallBuffer = new byte[4];
        byte[] readBuffer = new byte[256 * 256]; //68 KB

        int bytesRead = 0;

        byte currentID = 0;
        int currentToken = -1;
        int currentLength = -1;
        byte[] currentPayload;

        public Action<CommunicationHandler, byte, int, byte[]> OnEvent; //Change to 
        public void ResetProtocol()
        {
            currentID = 0;
            currentToken = -1;
            bytesRead = 0;
            currentPayload = null;
            currentLength = -1;
            bytesRead = 0;
            protocolLevel = 0;
        }

        bool handleUser_activeLastCall;
        public void HandleUser(TcpClient client)
        {
            var stream = client.GetStream();

            if (stream.DataAvailable)
            {
                if(User != null)
                    User.LastBeat = DateTime.Now;

                handleUser_activeLastCall = true;

                Debug.Write("\nReading handle cycle: ");
                if (protocolLevel == 0) //255 - Open flag
                {
                    var readBytes = stream.Read(smallBuffer, 0, 1);
                    if (readBytes != 1 && smallBuffer[0] != 255)
                        Debug.WriteLine($"Reading error: not ID {readBytes:N}");
                    else
                    {
                        Debug.Write(" O ");
                        protocolLevel++;
                    }
                }
                else if (protocolLevel == 1) //ID
                {
                    var readBytes = stream.Read(smallBuffer, 0, 1);
                    if (readBytes != 1)
                        Debug.WriteLine($"Reading error: not ID {readBytes:N}");
                    else
                    {
                        currentID = smallBuffer[0];
                        protocolLevel++;
                        Debug.Write($" ID {(UserEventType)currentID} ");
                    }
                }
                else if (protocolLevel == 2) //Token
                {
                    var readBytes = stream.Read(smallBuffer, 0, 4);
                    if (readBytes != 4)
                        Debug.WriteLine($"Reading error: not token {readBytes:N}");
                    else
                    {
                        currentToken = BitConverter.ToInt32(smallBuffer);
                        Debug.Write($" T {currentToken} \n");



                        // Check if we expect such Token + if event of such ID needs to be requested with a token
                        protocolLevel++;
                    }
                }
                else if (protocolLevel == 3) //Length
                {
                    var readBytes = stream.Read(smallBuffer, 0, 4);
                    if (readBytes != 4)
                        Debug.WriteLine($"Reading error: not length {readBytes:N}");
                    else
                    {
                        currentLength = BitConverter.ToInt32(smallBuffer);
                        Debug.Write($" L {currentLength} \n");
                        currentPayload = new byte[currentLength]; //TODO: inefficient
                        protocolLevel++;
                    }
                }
                else if (protocolLevel == 4) //Payload
                {
                    var remaining = currentLength - bytesRead;
                    var bufferSize = remaining < readBuffer.Length ? remaining : readBuffer.Length;

                    var bytes = stream.Read(readBuffer, 0, bufferSize);
                    for (int i = 0; i < bytes; i++)
                    {
                        currentPayload[bytesRead + i] = readBuffer[i];
                    }
                    bytesRead += bytes;
                    Debug.WriteLine($" => Reading {bytes:N} / {bytesRead:N} / {currentLength:N}");


                    if (currentLength == bytesRead)
                    {
                        //currentPayload = buffer;
                        protocolLevel++;
                    }
                    else if (currentLength < bytesRead)
                    {
                        Debug.WriteLine($"Reading error, payload reading exceeded... excpected bytes {currentLength:N}, recieved bytes {bytesRead:N}");
                    }
                }
                else if (protocolLevel == 5) //254 - Close flag
                {
                    Debug.Write(" E \n");
                    var readBytes = stream.Read(smallBuffer, 0, 1);
                    if (readBytes != 1 && smallBuffer[0] != 254)
                        Debug.WriteLine($"Reading error: not ID {readBytes}");
                    else
                    {
                        OnEvent(this, currentID, currentToken, currentPayload);
                        ResetProtocol();
                    }
                }
            }
            else if (User != null && User.EventQueue.Count > 0) //Otherwise Debug.Write
            {
                handleUser_activeLastCall = true;

                var eventData = User.EventQueue.Peek();

                byte[] payload = eventData.Payload;
                byte[] sendBuffer = new byte[1000 * 1000 * 250]; //Fixed buffer, TODO: change to a pool of buffers (pool per connection => 50 * 250 mb .. 12 500mb .. FFS

                var token = BitConverter.GetBytes(eventData.Token);

                var size = DataUtility.AddAt(sendBuffer, 0, eventData.ID, token, payload);

                Debug.WriteLine($"Sending ... {eventData.PayloadLength} ({size})");
                stream.Write(sendBuffer, 0, size);

                if (User.EventQueue.Count > 0)
                    User.EventQueue.Dequeue();
                else
                    Debug.ErrorLine("Command queue error: Queue is empty and it isn't suppose to be.");
            }
            else 
            { 
                handleUser_activeLastCall = false;
            }

            if (!handleUser_activeLastCall)
                Thread.Sleep(100);
        }
    }
    public class Event
    {
        public bool IsRequest; //true - request, false - response | Decides if token is generated or provided 
        public bool ExpectResponse;
        public bool Unique; //Doesn't make sense to queue multiple of the same type

        public UserEventType EventType { set { ID = (byte)value; } } // Helper for not dealing with numbers
        public byte ID;
        public int Token;
        public int PayloadLength => Payload.Length;
        public byte[] Payload;

        public int Size => 1 + 4 + 4 + PayloadLength; // Size of the actual transmition ... not including Open flag and close flag ( + 2 )

        public DateTime LastActivity; // When reading this is updated, used for resending events if they are never confirmed

        public DateTime CreationTime; // When the packet was created

        public Action OnConfirmed; // Fires when confirmation event with the same token arrives

        public Event()
        {
            LastActivity = CreationTime = DateTime.Now;
        }
    }
}
