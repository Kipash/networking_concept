﻿using System;
using System.Linq;

namespace Server
{
    public class DataUtility
    {
        public static int AddAt(byte[] buffer, int offset, byte ID, byte[] token, byte[] payload)
        {
            int currentOffset = offset;

            //255 - Open flag
            buffer[currentOffset] = 255;
            currentOffset += 1;

            //ID - 1 byte
            buffer[currentOffset] = ID;
            currentOffset += 1;

            //Token - 4 byte
            for (int tI = currentOffset, i = 0; tI < currentOffset + 4; tI++, i++)
            {
                buffer[tI] = token[i];
            }
            currentOffset += 4;

            //Length - 4 bytes
            int length = payload.Length;
            var lengthBytes = BitConverter.GetBytes(length);
            for (int lI = currentOffset, i = 0; lI < currentOffset + 4; lI++, i++)
            {
                buffer[lI] = lengthBytes[i];
            }
            currentOffset += 4;

            //Payload - Based on the Length
            for (int pI = currentOffset, i = 0; pI < currentOffset + length; pI++, i++)
            {
                if (buffer.Length - 1 < pI || payload.Length - 1 < i)
                    Debug.ErrorLine($"Error exeeded index => {pI}/{buffer.Length} .. {i}/{payload.Length}");
                buffer[pI] = payload[i];
            }
            currentOffset += length;

            //254 - Closing flag
            buffer[currentOffset] = 254;
            currentOffset += 1;

            return currentOffset;
        }




        const int wholeSizeBytewidth = 4;
        const int segmentSizeBytewidth = 4;

        public static byte[] CreateList(byte[][] data)
        {
            var allPayloads = data.Sum(x => x.Length);
            var totalSize = wholeSizeBytewidth + data.Length * segmentSizeBytewidth + allPayloads;
            var numOfSegments = data.Length;

            byte[] buffer = new byte[totalSize];

            int offset = 0;

            offset += Add(offset, buffer, BitConverter.GetBytes(numOfSegments));

            foreach (byte[] segment in data)
            {
                offset += Add(offset, buffer, BitConverter.GetBytes(segment.Length));
                offset += Add(offset, buffer, segment);
            }

            return buffer;
        }

        public static byte[][] ReadList(byte[] data, int initialOffset)
        {
            int offset = initialOffset;
            var numOfSegments = BitConverter.ToInt32(data, offset);
            offset += 4;

            byte[][] segments = new byte[numOfSegments][];
            for (int s = 0; s < numOfSegments; s++)
            {
                var segmentLength = BitConverter.ToInt32(data, offset);
                offset += 4;

                var buff = new byte[segmentLength];
                for (int i = 0; i < segmentLength; i++)
                {
                    if (offset + i > data.Length - 1)
                        Debug.ErrorLine("ReadList error: segment length differs from the data");
                    buff[i] = data[offset + i];
                }
                offset += segmentLength;
                segments[s] = buff;
            }

            if (data.Length - 1 == offset)
                Debug.ErrorLine($"ReadList error: read bytes {offset + 1}, total bytes {data.Length}");

            return segments;
        }

        static int Add(int offset, byte[] buffer, byte[] data)
        {
            for (int bufI = offset, i = 0; i < data.Length; bufI++, i++)
            {
                buffer[bufI] = data[i];
            }

            return data.Length;
        }
    }
}
