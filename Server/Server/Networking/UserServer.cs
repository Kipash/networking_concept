﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
    public enum UserEventType: byte  // 1-255!  => 0 is error state
    {
        //Essential events
        Authentication = 1,
        HeartBeat,
        Confirmation,

        //Exact events
        //RequestContent = 50,
        //AcceptContet,

        RequestContentManifest = 80,
        RequestTranslationManifest,
        RequestFace,
        RequestImage,
        RequestContent,


        ResponseContentManifest = 150,
        ResponseTranslationManifest,
        ResponseFace,
        ResponseImage,
        ResponseContent,

        //For testing purposes
        Message = 250,
    }

    public class UserServer
    {
        TcpListener listener;

        public UserServer()
        {
            RegisterHandlers();
        }

        public void StartServer(int port)
        {
            //IPAddress address = IPAddress.Parse("127.0.0.1");
            listener = new TcpListener(IPAddress.Any, port);

            listener.Start();

            Console.Title = $"SERVER ({listener.LocalEndpoint})";
            Debug.InfoLine($"Server started. Listening to all TCP clients at {port}");
        }

        //Accepts new connections
        //List<Thread> allConnectionThreads = new List<Thread>();
        public void Listen()
        {
            //Condition new connections => User limit
            while (true)
            {
                TcpClient client = listener.AcceptTcpClient();
                Debug.InfoLine("New connection...");
                var t = new Thread(() => {
                    HandleUserLoop(client);
                    Console.WriteLine("Connection thread terminated!");
                });
                t.Name = "SERVER_USER_Handle user connection";
                t.Start();

                //allConnectionThreads.Add(t);
            }
        }


        //Single connection handler
        void HandleUserLoop(TcpClient client)
        {
            Debug.InfoLine("Handle server loop starting...");
            var handler = new CommunicationHandler(client);
            handler.OnEvent = OnEvent;

            while (true)
            {
                try
                {
                    handler.HandleUser(client);
                }
                catch (Exception e)
                {
                    handler.ResetProtocol();
                    HandleConnectionClosed(client);
                    Debug.ErrorLine($"Handle server loop error: {e.GetType().Name}\n{e.Message}\n{e.StackTrace}");
                    break;
                }
            }
        }

        #region Handlers

        public delegate void OnEventHandler(User user, int token, byte[] payload);

        public Dictionary<TcpClient, User> Users = new Dictionary<TcpClient, User>();
        Dictionary<UserEventType, OnEventHandler> handlers = new Dictionary<UserEventType, OnEventHandler>();

        void RegisterHandlers()
        {
            RegisterHandler(UserEventType.HeartBeat, HandleHeartBeat);
            RegisterHandler(UserEventType.Confirmation, HandleConfirmation);
        }

        public void RegisterHandler(UserEventType type, OnEventHandler handler)
        {
            handlers.Add(type, handler);
        }

        void OnEvent(CommunicationHandler comHanlder, byte id, int token, byte[] payload)
        {
            if (Enum.IsDefined(typeof(UserEventType), id))
            {
                UserEventType eventType = (UserEventType)id;

                try
                {
                    if (eventType == UserEventType.Authentication) // GUID
                        HandleAuth(comHanlder, token, payload);
                    else
                    {
                        OnEventHandler handler = null;

                        if (!handlers.TryGetValue(eventType, out handler))
                        {
                            Debug.ErrorLine($"Error: no handler registered for ID {id}");
                            return;
                        }
                        if (comHanlder.User == null)
                        {
                            Debug.ErrorLine("User is null, never authenticated");
                            return;
                        }
                        if (!comHanlder.User.IsAuthed)
                        {
                            Debug.ErrorLine("User has invalid Guid");
                            return;
                        }

                        comHanlder.User.IsConnected = true;
                        handler(comHanlder.User, token, payload);
                    }
                }
                catch (Exception e)
                {
                    Debug.ErrorLine($"Error handeling event {id} - {e.GetType().Name}\n{e.Message}\n{e.StackTrace}");
                }
            }
            else
            {
                Debug.ErrorLine($"Unrecognized event ID: {id}");
            }
        }

        void HandleAuth(CommunicationHandler comHandler, int token, byte[] payload)
        {
            var client = comHandler.Client;

            try
            {
                var guid = new Guid(payload); //TODO: handle error state

                TcpClient key = null;
                User val = null;
                foreach (var x in Users)
                {
                    if (x.Value.ID == guid)
                    {
                        key = x.Key;
                        val = x.Value;
                        break;
                    }
                }

                if (key != null) //Updating old connections - dropping anything old
                {
                    val.IsConnected = true;

                    Users.Remove(key);
                    Users.Add(client, val);
                    comHandler.User = val;
                    Debug.WarningLine($"User reauthenticated: {guid}");
                    val.LastBeat = DateTime.Now;
                    SendConfirm(val, token, true, true);
                }
                else
                {
                    var user = new User() { ID = guid, TcpClient = client };
                    user.IsConnected = true;
                    user.LastBeat = DateTime.Now;

                    Users.Add(client, user);
                    comHandler.User = user;
                    Debug.WarningLine($"User authenticated: {guid}");

                    SendConfirm(user, token, true, true);
                }

                Debug.InfoLine($"CCU: {Users.Count}");
            }
            catch (Exception e)
            {
                client.Client.Disconnect(true);
                client.GetStream().Dispose();
                client.Dispose();
                Debug.ErrorLine($"Auth error: {e.GetType().Name}\n{e.Message}\n{e.StackTrace}");
            }
        }
        public void HandleConnectionClosed(User user)
        {
            user.IsConnected = false;
            Debug.WarningLine($"Client had timeout - {user.ID}");
            HandleConnectionClosed(user.TcpClient);
        }
        public void HandleConnectionClosed(TcpClient client)
        {
            if (Users.ContainsKey(client))
            {
                Users[client].IsConnected = false;
            }
            else
                Debug.ErrorLine("Disconnected client has no ID");
        }

        /// <summary>
        /// Sends a confirmation event
        /// </summary>
        /// <param name="token"></param>
        /// <param name="sucess"></param>
        /// <param name="role">false - response, true - request | POV of the sender</param>
        public void SendConfirm(User s, int token, bool sucess, bool role)
        {
            s.RegisterEvent(new Event()
            {
                EventType = UserEventType.Confirmation,
                IsRequest = role,
                ExpectResponse = false,
                Token = token,
                Payload = new byte[] { (byte)(sucess ? 1 : 0), (byte)(role ? 1 : 2) },
            });
        }
        public void Confirm(User s, int token)
        {
            if (!s.Requests.Remove(token))
                Debug.WarningLine($"Token {token} can't be confirmed and request can't be removed.");
        }


        void HandleHeartBeat(User sender, int token, byte[] payload)
        {
            SendConfirm(sender, token, true, true);

            sender.LastBeat = DateTime.Now;
            sender.BeatCount++;
        }

        void HandleConfirmation(User user, int token, byte[] payload)
        {
            Debug.InfoLine("Conf");
            if (payload.Length == 2)
            {
                var ok = payload[0];
                if (!(ok == 0 || ok == 1))
                {
                    Debug.ErrorLine($"Response state is invalid {ok}");
                    return;
                }

                var role = payload[1];

                Dictionary<int, Event> events = null;
                if (role == 1)
                    events = user.Requests;
                else if (role == 2)
                    events = user.Responses;

                if (events != null)
                {
                    if (events.TryGetValue(token, out Event eventData))
                    {
                        if (ok == 1)
                        {
                            if (IsEvent(eventData.ID, UserEventType.RequestContent, UserEventType.Message))
                            {
                                Debug.InfoLine($"Relevant event confirmed {(UserEventType)eventData.ID}, {role}, Expect Response: {eventData.ExpectResponse}");
                            }

                            eventData.OnConfirmed?.Invoke();
                            if (role == 2)
                            {
                                user.Requests.Remove(token);
                                user.Responses.Remove(token);

                                Debug.InfoLine("Removing from all buffers");
                            }
                            else
                            {
                                if (!eventData.ExpectResponse)
                                    events.Remove(token);
                            }
                        }
                        else
                        {
                            eventData.Token = 0; //will be regenerated
                            user.RegisterEvent(eventData);
                            Debug.WarningLine("Event was unsuccessful ... resending!");
                        }
                    }
                    else
                        Debug.ErrorLine($"Unknown event has {(ok == 0 ? "unsuccessful" : "successful")} confirmation"); //?
                }
                else
                    Debug.ErrorLine($"Unknown role {role}");
            }
            else
                Debug.ErrorLine($"Confirmation has invalid payload of length {payload.Length}");
        }

        #endregion


        public static bool IsEvent(byte ID, params UserEventType[] types)
        {
            foreach (var x in types)
            {
                if (ID == (byte)x)
                    return true;
            }
            return false;
        }
    }


    public class User
    {
        public TcpClient TcpClient;
        public Guid ID = Guid.Empty;

        static Random random = new Random();

        public Queue<Event> EventQueue = new Queue<Event>();
        public Dictionary<int, Event> Requests  = new Dictionary<int, Event>();
        public Dictionary<int, Event> Responses = new Dictionary<int, Event>();

        public bool IsAuthed => ID != Guid.Empty;

        //Alive check
        public DateTime LastBeat;
        public int BeatCount;

        public bool IsConnected;

        public void RegisterEvent(Event e)
        {
            var q = EventQueue;

            if (UserServer.IsEvent(e.ID, UserEventType.Confirmation))
            {
                q.Enqueue(e);
                return;
            }

            if (e.Unique)
            {
                if (Requests.Values.Any(x => x.ID == e.ID) || Responses.Values.Any(x => x.ID == e.ID)) //Performance?   
                {
                    Debug.ErrorLine("Anti-spam prevention");
                    return;
                }
            }

            if (e.IsRequest)
            {
                if (Requests.ContainsValue(e))
                    Requests.Remove(e.Token);

                if (e.Token == 0)
                    e.Token = random.Next(0, int.MaxValue);

                Requests.Add(e.Token, e);
            }
            else
            {
                if (Responses.ContainsValue(e))
                    Responses.Remove(e.Token);

                Responses.Add(e.Token, e);
            }

            q.Enqueue(e);
        }
    }
}
