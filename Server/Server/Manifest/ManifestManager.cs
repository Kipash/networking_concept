﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Server.Manifest
{
    public class ManifestManager
    {
        
        const string AppManifestPath = "App manifest.json";
        const string ContentManifestPath = @"Content\";
        //const string TranslationDataPath = "Translation/";

        public AppManifest AppManifest;
        public List<SampleInfo> SampleInfos = new List<SampleInfo>();

        public ManifestManager()
        {
            LoadManifests();
        }

        void LoadManifests()
        {
            AppManifest = LoadOrCreateManfest<AppManifest>(AppManifestPath);
            LoadSampleInfos();
        }

        void LoadSampleInfos()
        {
            foreach(var dir in Directory.GetDirectories(ContentManifestPath))
            {
                var jsonPath = $@"{dir}\Manifest.json";
                var info =  LoadOrCreateManfest<SampleInfo>(jsonPath);
                info.DirectoryPath = dir;
                SampleInfos.Add(info);
            }
        }

        static JsonSerializerSettings jsonSettings = new JsonSerializerSettings()
        {
            DefaultValueHandling = DefaultValueHandling.Populate,
            NullValueHandling = NullValueHandling.Include,
            ObjectCreationHandling = ObjectCreationHandling.Replace,
            Formatting = Formatting.Indented
        };

        public static T LoadOrCreateManfest<T>(string path) where T: new()
        {
            if(File.Exists(path))
            {
                var txt = File.ReadAllText(path);
                T loadedT = JsonConvert.DeserializeObject<T>(txt, jsonSettings);
                if (loadedT != null)
                    return loadedT;
            }
            
            var t = new T();
            var json = JsonConvert.SerializeObject(t, jsonSettings);
            File.WriteAllText(path, json);
            return t;
        }
    }
}
