﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server.Manifest
{
    public class AppManifest
    {
        public AvatarSDKManifest AvatarSDK = new AvatarSDKManifest();

        public int UserPort;
        public int GreenScreenPort;
    }

    public class AvatarSDKManifest
    {
        public string GrantType = "";
        public string ClientID = "";
        public string ClientSecret = "";
        public string Pipeline = "";
        public string Pipeline_subtype = "";
        public int APIWorkerCount = 1;
    }
}
