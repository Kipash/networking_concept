﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Server.Manifest
{
    public class SampleInfo
    {
        public string ID = ""; //Addressable ID 
        public int Type = -1; //Image target, model target, plane detection
        //public string Description; //Debug info, binds human description to the ID
        public int AccessLevel = -1; // Production / Testing / Dev
        public string TargetPath = "";
        public string ABPath_Android = "";
        public string ABPath_iOS = "";

        [NonSerialized]
        public string DirectoryPath;
    }
}
