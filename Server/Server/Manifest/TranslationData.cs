﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server.Manifest
{
    public class TranslationData
    {
        public string Name = "";
        public Dictionary<string, string> TranslationKeys = new Dictionary<string, string>();
    }
}
