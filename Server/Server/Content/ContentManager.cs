﻿using Server.Manifest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Server.Content
{
    public class ContentManager
    {
        UserServer server;
        Dictionary<string, Sample> samples = new Dictionary<string, Sample>();

        byte[] infoManifest;

        public ContentManager(UserServer s)
        {
            server = s;

            //Response
            server.RegisterHandler(UserEventType.RequestContentManifest, (user, token, payload) => 
            {
                s.SendConfirm(user, token, true, true);

                user.RegisterEvent(new Event()
                {
                    EventType = UserEventType.ResponseContentManifest,
                    IsRequest = false,
                    Token = token,
                    Payload = infoManifest
                });
            });

            //Response
            server.RegisterHandler(UserEventType.RequestContent, (user, token, payload) =>
            {
                s.SendConfirm(user, token, true, true);

                var id = Encoding.ASCII.GetString(payload);

                user.RegisterEvent(new Event()
                {
                    EventType = UserEventType.ResponseContent,
                    IsRequest = false,
                    Token = token,
                    Payload = samples[id].Content_Android
                });
            });
        }

        public void LoadSamples(IEnumerable<SampleInfo> infos)
        {
            Debug.InfoLine("Loading samples:");
            foreach(var info in infos)
            {
                var sample = new Sample();
                sample.ID = info.ID;
                sample.Type = info.Type;
                sample.AccessLevel = info.AccessLevel;

                Debug.InfoLine($" > {sample.ID}");

                var targetPath = $@"{info.DirectoryPath}\{info.TargetPath}";
                if (File.Exists(targetPath))
                    sample.Target = File.ReadAllBytes(targetPath);
                else
                    throw new Exception($"({info.DirectoryPath}) {info.ID}: invalid Target path");

                var ab_android = $@"{info.DirectoryPath}\{info.ABPath_Android}";
                if (File.Exists(ab_android))
                    sample.Content_Android = File.ReadAllBytes(ab_android);
                else
                    throw new Exception($"({info.DirectoryPath}) {info.ID}: invalid ABPath_Android");

                var ab_ios = $@"{info.DirectoryPath}\{info.ABPath_iOS}";
                if (File.Exists(ab_ios))
                    sample.Content_iOS = File.ReadAllBytes(ab_ios);
                else
                    throw new Exception($"({info.DirectoryPath}) {info.ID}: invalid ABPath_iOS");

                samples.Add(sample.ID, sample);
            }

            infoManifest = BakeInfo();
        }
        byte[] BakeInfo()
        {
            var list = samples.Select(sample => 
                                    DataUtility.CreateList(new byte[][] { 
                                        sample.Key.Select(x => (byte)x).ToArray(),      //ID
                                        sample.Value.Target,                            //Target
                                        BitConverter.GetBytes(sample.Value.Type),       //Type
                                        BitConverter.GetBytes(sample.Value.AccessLevel) //Access level
                                      }))
                              .ToArray();

            var data = DataUtility.CreateList(list);

            return data;
        }
    }



    public class Sample
    {
        public string ID;
        public int Type;
        public int AccessLevel;
        public byte[] Target;
        public byte[] Content_Android;
        public byte[] Content_iOS;
    }
}
