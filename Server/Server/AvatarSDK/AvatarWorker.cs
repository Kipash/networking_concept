﻿using Server.Manifest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Server.AvatarSDK
{
    public class AvatarWorker
    {
        public Queue<AvatarRequest> RequestQueue = new Queue<AvatarRequest>();
        AvatarCommunicationHandler handler;

        public Action<AvatarRequest, bool, byte[], byte[]> OnResult;

        public void Intialize(AvatarSDKManifest cfg)
        {
            handler = new AvatarCommunicationHandler(cfg);
            handler.OnError = (msg) => Debug.ErrorLine($"AvatarSDK error: {msg}");
            handler.OnUnauthorized = () => {
                Debug.ErrorLine($"AvatarSDK error: Unauthorized");
                evaluateQueue = false;
            };

            var t = new Thread(() => StartHandler());
            t.Name = "SERVER_AVATARSDK_Main";
            t.Start();
        }

        bool evaluateQueue;
        bool errorFlag;
        void StartHandler()
        {
            while (true)
            {
                try
                {
                    var auth = handler.Authenticate();
                    var id = handler.GetPlayerID(auth);
                    evaluateQueue = true;

                    Debug.WriteLine("starting avatar queue loop...");
                    while (evaluateQueue)
                    {
                        AvatarQueueHandler(id, auth);
                        Thread.Sleep(200);
                    }
                    Debug.WriteLine("stopping avatar queue loop...");
                }
                catch (Exception e)
                {
                    Debug.ErrorLine($"AvatarSDK: {e.GetType().Name}\n{e.Message}\n{e.StackTrace}");
                }
                Thread.Sleep(500);
            }
        }
        void AvatarQueueHandler(string id, string auth)
        {
            if (RequestQueue.Count > 0)
            {
                errorFlag = false;
                var request = RequestQueue.Peek();

                var headRequest = handler.UploadSelfie(id, auth, request.Image);

                HeadProgress progress;
                while (true)
                {
                    progress = handler.GetStatus(id, auth, headRequest.url);
                    if (progress.progress == 100)
                        break;

                    if (progress.status == "Failed" || progress.status == "Timed Out") //Error state
                    {
                        OnResult?.Invoke(request, false, null, null);
                        RequestQueue.Dequeue();
                        return;
                    }

                    Thread.Sleep(2000);
                }

                var texture = handler.Download(id, auth, progress.texture);
                var zippedMesh = handler.Download(id, auth, progress.mesh);
                var mesh = handler.Unzip(zippedMesh);

                OnResult?.Invoke(request, true, texture, mesh);

                RequestQueue.Dequeue();
            }
        }
    }
}
