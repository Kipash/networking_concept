﻿using Newtonsoft.Json;
using RestSharp;
using Server.Manifest;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;

namespace Server.AvatarSDK
{
    public class AvatarCommunicationHandler
    {
        public Action OnUnauthorized;
        public Action<string> OnError;

        AvatarSDKManifest cfg;
        public AvatarCommunicationHandler(AvatarSDKManifest cfg)
        {
            this.cfg = cfg;
        }

        const int MaxRequestsPerMinute = 60;
        int numOfRequets;
        int currentMinute = -1;
        void ThrottleRequests()
        {
            while (numOfRequets > MaxRequestsPerMinute)
            {
                if (currentMinute != DateTime.Now.Minute)
                {
                    currentMinute = DateTime.Now.Minute;
                    numOfRequets = 0;
                }
                Thread.Sleep(500);
            }

            numOfRequets++;
        }

        public string Authenticate()
        {
            ThrottleRequests();

            var c = new RestClient("https://api.avatarsdk.com/o/token/");
            var r = new RestRequest(Method.POST);

            //creds from avatarsdk.com account
            r.AddParameter("grant_type", cfg.GrantType);
            r.AddParameter("client_id", cfg.ClientID);
            r.AddParameter("client_secret", cfg.ClientSecret);

            IRestResponse res = c.Execute(r);
            var responseString = res.Content;

            if (res.StatusCode != HttpStatusCode.OK)
                OnError?.Invoke(responseString);

            var auth = JsonConvert.DeserializeObject<AuthJson>(responseString);
            Debug.WriteLine($"Auth: \n{responseString}");

            return $"{auth.token_type} {auth.access_token}";
        }

        public string GetPlayerID(string auth)
        {
            ThrottleRequests();

            var c = new RestClient("https://api.avatarsdk.com/players/");
            var r = new RestRequest(Method.POST);

            r.AddHeader("Authorization", auth);

            IRestResponse res = c.Execute(r);
            var responseString = res.Content;

            if (res.StatusCode == HttpStatusCode.Unauthorized)
                OnUnauthorized?.Invoke();
            else if (res.StatusCode != HttpStatusCode.Created)
                OnError?.Invoke(responseString);

            var id = JsonConvert.DeserializeObject<IDJson>(responseString);
            Debug.WriteLine($"ID: \n{responseString}");

            return id.code;
        }

        public HeadRequest UploadSelfie(string id, string auth, byte[] image)
        {
            ThrottleRequests();

            var c = new RestClient("https://api.avatarsdk.com/avatars/");
            var r = new RestRequest(Method.POST);

            r.AddHeader("Authorization", auth);
            r.AddHeader("X-PlayerUID", id);

            r.AddParameter("pipeline", cfg.Pipeline);
            r.AddParameter("pipeline_subtype", cfg.Pipeline_subtype);
            r.AddParameter("name", $"ServerTest");

            var imgBytes = File.ReadAllBytes(@"C:\Users\Krystof\Downloads\20200704_174914.jpg");

            r.AddFileBytes("photo", image, "Selfie.jpg");

            IRestResponse res = c.Execute(r);
            var responseString = res.Content;

            if (res.StatusCode == HttpStatusCode.Unauthorized)
                OnUnauthorized?.Invoke();
            else if (res.StatusCode != HttpStatusCode.Created)
                OnError?.Invoke(responseString);

            Debug.WriteLine($"Upload: \n{responseString}");
            return JsonConvert.DeserializeObject<HeadRequest>(responseString);
        }

        public HeadProgress GetStatus(string id, string auth, string url)
        {
            ThrottleRequests();

            var c = new RestClient(url);
            var r = new RestRequest(Method.GET);

            r.AddHeader("Authorization", auth);
            r.AddHeader("X-PlayerUID", id);

            IRestResponse res = c.Execute(r);
            var responseString = res.Content;

            if (res.StatusCode == HttpStatusCode.Unauthorized)
                OnUnauthorized?.Invoke();
            else if (res.StatusCode != HttpStatusCode.OK)
                OnError?.Invoke(responseString);

            Debug.WriteLine($"Get status: \n{responseString}");
            return JsonConvert.DeserializeObject<HeadProgress>(responseString);
        }

        public byte[] Download(string id, string auth, string url)
        {
            ThrottleRequests();

            var c = new RestClient(url);
            var r = new RestRequest(Method.GET);

            r.AddHeader("Authorization", auth);
            r.AddHeader("X-PlayerUID", id);

            IRestResponse res = c.Execute(r);

            if (res.StatusCode == HttpStatusCode.Unauthorized)
                OnUnauthorized?.Invoke();
            else if (res.StatusCode != HttpStatusCode.OK)
                OnError?.Invoke(res.Content);

            var bytes = res.RawBytes;
            Debug.WriteLine($"Downloaded {bytes.Length:N} bytes");

            return bytes;
        }

        public byte[] Unzip(byte[] zipped)
        {
            var ms = new MemoryStream(zipped);
            var za = new ZipArchive(ms);
            var e = za.Entries.First();
            Console.WriteLine(e.FullName);
            var stream = e.Open();

            byte[] bytes = new byte[e.Length];
            int offset = 0;
            int offsetLastTick = -1;
            while (offsetLastTick != offset)
            {
                offsetLastTick = offset;
                int diff = (int)e.Length - offset;
                int readSize = diff < 1024 ? diff : 1024;
                offset += stream.Read(bytes, offset, readSize);
            }
            stream.Close();
            stream.Dispose();


            Debug.WriteLine($"Unzipped {bytes.Length:N} bytes");
            return bytes;
        }
    }



    #region AvatarSDK Json objects
    public class AuthJson
    {
        public string access_token;
        public string token_type;
        public int expires_in;
        public string scope;
    }

    public class IDJson
    {
        public string url;
        public string code;
        public string created_on;
        public string comment;
    }

    public class HeadRequest
    {
        public string url;
        public string code;
        public string status;
        public int progress;
        public string created_on;
        public string name;
        public string description;
    }

    public class HeadProgress
    {
        public string url;
        public string code;
        public string status;
        public int progress;
        public string name;
        public string description;
        public string created_on;
        public string ctime;
        public string model_info;
        public string thumbnail;
        public string mesh;
        public string texture;
        public string preview;
        public string haircuts;
        public string blendshapes;
        public string pipeline;
        public string pipeline_subtype;
    }
    #endregion
}
