﻿using Server.Manifest;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;

namespace Server.AvatarSDK
{
    public class AvatarManager
    {
        AppManifest configuration;
        List<AvatarWorker> workers = new List<AvatarWorker>();
        public AvatarManager(AppManifest cfg, UserServer server)
        {
            configuration = cfg;

            var workerCount = cfg.AvatarSDK.APIWorkerCount;
            if (workerCount < 1)
                workerCount = 1;

            Debug.WriteLine($"AvatarSDK: starting {workerCount} workers");

            for (int i = 0; i < workerCount; i++)
            {
                var w = new AvatarWorker();
                w.Intialize(cfg.AvatarSDK);
                w.OnResult = (request, success, texture, mesh) => {
                    if (success)
                        SendFinishedHead(request, texture, mesh);
                    else
                        SendErrorHead(request);
                };
                workers.Add(w);
            }

            server.RegisterHandler(UserEventType.RequestFace, (user, token, payload) =>
            {
                server.SendConfirm(user, token, true, true);
                AddRequest(new AvatarRequest()
                {
                    Image = payload,
                    Token = token,
                    User = user
                });
            });
        }

        void AddRequest(AvatarRequest request)
        {
            int smallestQ = int.MaxValue;
            AvatarWorker worker = null;
            foreach (var x in workers)
            {
                var count = x.RequestQueue.Count;
                if (count < smallestQ)
                {
                    smallestQ = count;
                    worker = x;
                }
            }

            Debug.WarningLine($"WORKER: {workers.IndexOf(worker)}");
            worker.RequestQueue.Enqueue(request);
        }
       

        void SendFinishedHead(AvatarRequest request, byte[] texture, byte[] mesh)
        {
            Debug.WarningLine($"Head: texture {texture.Length:N} mb, mesh {mesh.Length:N} mb");

            var data = DataUtility.CreateList(new byte[][] { texture, mesh });
            request.User.RegisterEvent(new Event()
            {
                EventType = UserEventType.ResponseFace,
                IsRequest = false,
                Token = request.Token,
                Payload = data
            });
        }
        void SendErrorHead(AvatarRequest request)
        {
            request.User.RegisterEvent(new Event()
            {
                EventType = UserEventType.ResponseFace,
                IsRequest = false,
                Token = request.Token,
                Payload = new byte[] { 0 }
            });
        }
    }

    public class AvatarRequest
    {
        public User User;
        public int Token;
        public byte[] Image;
    }
}
